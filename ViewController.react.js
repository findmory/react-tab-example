var React = require('react');
var ViewSelector= require('./ViewSelector.react');

var ViewController = React.createClass({
  getInitialState: function(){
    return {view: 1}
  },
  butHandle: function(btnNum, event){
    console.log("you clicked: ", btnNum);
    this.setState({view:btnNum});
  },
  render: function(){
    return (
      <div>
        <button onClick={this.butHandle.bind(this,1)} className={this.state.view==1?"selected":null}>View1</button>
        <button onClick={this.butHandle.bind(this,2)} className={this.state.view==2?"selected":null}>View2</button><br />
        <ViewSelector view = {this.state.view} />
      </div>
    );
  }
});

module.exports = ViewController;