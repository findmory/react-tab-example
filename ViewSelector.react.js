var React = require('react');
var View1 = require('./View1.react');
var View2 = require('./View2.react');

var ViewSelector = React.createClass({
  render: function(){
    if (this.props.view == 1){
      return <View1 />
    }else {
      return <View2 />
    }
  }
});

module.exports = ViewSelector;