'use strict';

var React = require('react');
var ReactDOM = require('react-dom');
var ViewController = require('./ViewController.react');

var App = React.createClass({
  render: function() {  
    return <ViewController />;
  }
});


var start = new Date().getTime();


ReactDOM.render(
  <App  />,
  document.getElementById('container')
);

